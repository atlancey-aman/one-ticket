package com.atlancey.oneticket;

import android.graphics.Bitmap;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

/*
 * Created by DuDe DhRuVa on 17-08-2018.
 */

public class Ticket_QR_Return
{
    public BitMatrix bM;
    public Bitmap x;
    public Bitmap return_qr(String s)
    {


        MultiFormatWriter mfw=new MultiFormatWriter();
        try
        {
           bM=mfw.encode(s, BarcodeFormat.QR_CODE,200,200);
            BarcodeEncoder bce=new BarcodeEncoder();
             x=bce.createBitmap(bM);


        }
        catch(WriterException e)
        {e.printStackTrace();
        }
         return x;
    }
}
