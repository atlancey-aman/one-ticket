package com.atlancey.oneticket;

import android.content.Intent;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    Intent i;
    TextView name;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
       PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString("user_name","Subhadeep").apply();
        name=(TextView)findViewById(R.id.textView4);
        String u_name=PreferenceManager.getDefaultSharedPreferences(this).getString("user_name","");
        if(u_name.length()==0)
        {
            name.setText("");
            i=new Intent(this,LoginActivity.class);
        }
        else
            {
                name.setText(u_name);
                i=new Intent(this,MainMenu.class);
            }
                Thread t = new Thread() {
                  public void run() {
                    try
                     {
                        Thread.sleep(4000);
                        startActivity(i);
                     }
                    catch (InterruptedException e)
                    {
                        startActivity(i);
                    }
                }
            };
            t.start();
    }
}
