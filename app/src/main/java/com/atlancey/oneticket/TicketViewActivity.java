package com.atlancey.oneticket;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

public class TicketViewActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {
    GridView ticket_view_list;
    String[] destination = {"Dwarka Sec 9","Kashmiri Gate"};
    String[] source = {"Chandni Chowk","New Delhi"};
    String[] info = {"next 5:15 pm","next 6:00 pm"};
    int[] image_transit = {R.drawable.bus,R.drawable.metro};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ticket_view);
        ticket_view_list = findViewById(R.id.ticket_view_list);
        MyAdapter adapter = new MyAdapter(this, destination, source, info, image_transit);
        ticket_view_list.setAdapter(adapter);
        ticket_view_list.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    class MyAdapter extends ArrayAdapter {
        int[] imageArray;
        String[] destinationArray;
        String[] sourceArray;
        String[] info;

        public MyAdapter(Context c, String[] source, String[] destination, String[] info, int[] image_transit) {
            super(c, R.layout.ticket_view, R.id.textView_source, source);
            this.imageArray = image_transit;
            this.sourceArray = source;
            this.info = info;
            this.destinationArray = destination;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = inflater.inflate(R.layout.ticket_view, parent, false);
            TextView tvs = (TextView) v.findViewById(R.id.textView_source);
            TextView tvd = (TextView) v.findViewById(R.id.textView_destination);
            TextView tvi = (TextView) v.findViewById(R.id.textView_info);
            ImageView iv = (ImageView) v.findViewById(R.id.image_transit);
            tvs.setText(sourceArray[position]);
            tvd.setText(destinationArray[position]);
            tvi.setText(info[position]);
            iv.setImageResource(image_transit[position]);
            return v;

        }
    }
}
