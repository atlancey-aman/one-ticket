package com.atlancey.oneticket;

import android.content.Context;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import android.support.v4.view.GravityCompat;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Button;

import java.util.ArrayList;

public class MainMenu extends AppCompatActivity implements AdapterView.OnItemClickListener, View.OnClickListener {


    String[] branch = {"Some Random String"};
    String b = "";
    GridView lv1;
    String k;
    int[] icon = {R.drawable.bus};
    RelativeLayout r1,r2;
    Boolean hi=true;
    Button buy_metro,buy_bus,buy_train,find_route;
    Intent i;
    TextView username,balance;
    ImageView add_balance;



    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        buy_metro = findViewById(R.id.buy_metro);
        buy_bus = findViewById(R.id.buy_bus);
        buy_train = findViewById(R.id.buy_train);
        find_route = findViewById(R.id.route_finder);
        username = findViewById(R.id.username);
        add_balance = findViewById(R.id.add_balance);

        buy_metro.setOnClickListener(this);
        buy_bus.setOnClickListener(this);
        buy_train.setOnClickListener(this);
        find_route.setOnClickListener(this);
        username.setOnClickListener(this);
        add_balance.setOnClickListener(this);
        balance = findViewById(R.id.balance);

        r1=(RelativeLayout)findViewById(R.id.R1);
        r2=(RelativeLayout)findViewById(R.id.R2);
        r1.setVisibility(View.VISIBLE);
        r2.setVisibility(View.GONE);

        lv1 = (GridView) findViewById(R.id.gv_list);
        MyAdapter adapter = new MyAdapter(this, branch, icon);
        lv1.setAdapter(adapter);
        lv1.setOnItemClickListener(this);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        int balance=100;
        }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.action_bar_items,menu);
        if (hi)
        {
            menu.findItem(R.id.exit_menues).setVisible(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("YOUR TICKETS");

        }
        else
        {
            menu.findItem(R.id.exit_menues).setVisible(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setTitle("ONE TICKET");
        }
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
       switch(item.getItemId())
       {

           case R.id.exit_menues:
               hi=true;
               r1.setVisibility(View.VISIBLE);
               r2.setVisibility(View.GONE);
               invalidateOptionsMenu();
               break;
           case android.R.id.home:
               hi=false;
               r2.setVisibility(View.VISIBLE);
               r1.setVisibility(View.GONE);
               invalidateOptionsMenu();
               break;

       }

       return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        String s=branch[position];
        Intent i=new Intent(this,TicketViewActivity.class);
        i.putExtra("Key",s);
        startActivity(i);

    }

    @Override
    public void onClick(View v) {
        if (v == buy_metro) {
            i = new Intent(this,MetroTicketActivity.class);
            startActivity(i);
        } else if (v == buy_bus) {
            i = new Intent(this,BusTicketActivity.class);
            startActivity(i);
        } else if (v == buy_train) {
            Toast.makeText(this,"Buy Train Ticket",Toast.LENGTH_SHORT).show();
        } else if (v == find_route) {
            Toast.makeText(this,"Find Route",Toast.LENGTH_SHORT).show();
        } else if (v == username) {
            i = new Intent(this,UserProfileActivity.class);
            startActivity(i);
        } else if (v == add_balance) {
            balance.setText("100");
        }
    }

    class MyAdapter extends ArrayAdapter {
        int[] imageArray;
        String[] titleArray;

        public MyAdapter(Context c, String[] branch1, int[] icon1) {
            super(c, R.layout.ticket_list, R.id.textView, branch1);
            //here the error will generate due to ticket_list.xml
            this.imageArray = icon1;
            this.titleArray = branch1;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = inflater.inflate(R.layout.ticket_list, parent, false);
            TextView tv = (TextView) v.findViewById(R.id.textView);
            ImageView iv = (ImageView) v.findViewById(R.id.imageView);
            tv.setText(titleArray[position]);
            iv.setImageResource(imageArray[position]);
            return v;

        }
    }

}