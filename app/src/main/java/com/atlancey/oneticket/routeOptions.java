package com.atlancey.oneticket;
//this activity is for displaying the options of routes returned from the server

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

public class routeOptions extends AppCompatActivity {
    ArrayList<routeOptions_getterAndSetter> reviewList;
    routeOptions_helperClass adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route_options);

        reviewList = new ArrayList<routeOptions_getterAndSetter>();
        GridView gridView = findViewById(R.id.route_options);
        adapter = new routeOptions_helperClass(getApplicationContext(), R.layout.route_options, reviewList);
        gridView.setAdapter(adapter);


        String jsonReceived = "{\"destination\": \"long,lat\",\"options\": [{\"time\": \"in mins\",\"steps\": [{\"number\": \"yellow\",\"stop\":\"station_name\",\"start\": \"station_name\",\"type\": \"metro\"},{\"number\": \"x1\",\"stop\":\"station_name\",\"start\": \"station_name\",\"type\": \"bus\"}],\"cost\": \"in rs\"},{\"time\": \"in mins\",\"steps\": [{\"number\": \"yellow\",\"stop\": \"station_name\",\"start\": \"station_name\",\"type\": \"metro\"},{\"number\": \"x1\",\"stop\": \"station_name\",\"start\": \"station_name\",\"type\": \"bus\"}],\"cost\": \"in rs\"},{\"time\": \"in mins\",\"steps\":[{\"number\": \"yellow\",\"stop\": \"station_name\",\"start\": \"station_name\",\"type\": \"metro\"},{\"number\": \"x1\",\"stop\": \"station_name\",\"start\": \"station_name\",\"type\": \"bus\"}],\"cost\": \"in rs\"}],\"source\": \"long,lat\"}";
        try {
            JSONObject jsono = new JSONObject(jsonReceived);
            if(!jsono.getString("title").isEmpty()) {
                Toast.makeText(getApplicationContext(), jsono.getString("source"), Toast.LENGTH_LONG).show();
            }
            else
            {Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();}

            JSONArray jarray = jsono.getJSONArray("options");

            //this for is for getting options available for commute
            for (int i = 0; i < jarray.length(); i++) {
                JSONObject object = jarray.getJSONObject(i);

                for (int j = 0; j < object.length(); i++) {
                    JSONArray jsonArray = object.getJSONArray("steps");

                    for (int k = 0; k < jsonArray.length(); k++) { //access each value in the steps part to set here
                             /*set the "number":"yellow",
                             "stop":"station_name",
                                 "start":"station_name",
                                 "type":"metro"*/


                        Toast.makeText(getApplicationContext(), object.getString("time"), Toast.LENGTH_SHORT).show();
                        Thread t = new Thread() {
                            public void run() {
                                try {
                                    Thread.sleep(4000);
                                    Toast.makeText(getApplicationContext(), "******", Toast.LENGTH_SHORT).show();

                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                        };
                        t.start();


                   /* reviews rev = new reviews();
                    //getting json object values from json array
                    rev.setTitle(object.getString("title"));
                    rev.setComment(object.getString("comment"));
                    rev.setUsefulness(object.getString("usefulness"));
                    //getting value within json object


                    JSONObject numstar = object.optJSONObject("ratings");
                    String sta = numstar.getString("Overall");
                    rev.setStars(sta);

                    //adding data to the arraylist
                    reviewList.add(rev);
*/
                    }

                }

                //------------------>>

            } /*catch (JSONException e2) {
            e2.printStackTrace();
            Toast.makeText(getApplicationContext(),"Error in parsing json object",Toast.LENGTH_LONG).show();
        }*/
        /*catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }*/


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
