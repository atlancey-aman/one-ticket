package com.atlancey.oneticket;

/**
 * Created by DuDe DhRuVa on 25-08-2018.
 */
//this class is for setting the values to show the list of route options and intermediate values/steps

public class routeOptions_getterAndSetter
{
    String typeOfJourney; //overall  00
    String fullTime; //total time  00
    String fullCost;  //total cost  00
    String partTime;  //Small part Time   00
    String partCost;  //small part cost 00
    String steps_number;  // train num or line color  00
    String steps_start;   //mid way source  00
    String steps_stop;    //mid way destination  00
    String steps_type;   //type of travel, bus train ,metro00

    public String getTypeOfJourney() {
        return typeOfJourney;
    }

    public void setTypeOfJourney(String typeOfJourney) {
        this.typeOfJourney = typeOfJourney;
    }

    public String getFullTime() {
        return fullTime;
    }

    public void setFullTime(String fullTime) {
        this.fullTime = fullTime;
    }

    public String getFullCost() {
        return fullCost;
    }

    public void setFullCost(String fullCost) {
        this.fullCost = fullCost;
    }

    public String getPartTime() {
        return partTime;
    }

    public void setPartTime(String partTime) {
        this.partTime = partTime;
    }

    public String getPartCost() {
        return partCost;
    }

    public void setPartCost(String partCost) {
        this.partCost = partCost;
    }

    public String getSteps_number() {
        return steps_number;
    }

    public void setSteps_number(String steps_number) {
        this.steps_number = steps_number;
    }

    public String getSteps_start() {
        return steps_start;
    }

    public void setSteps_start(String steps_start) {
        this.steps_start = steps_start;
    }

    public String getSteps_stop() {
        return steps_stop;
    }

    public void setSteps_stop(String steps_stop) {
        this.steps_stop = steps_stop;
    }

    public String getSteps_type() {
        return steps_type;
    }

    public void setSteps_type(String steps_type) {
        this.steps_type = steps_type;
    }



}