package com.atlancey.oneticket;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class FindRouteActivity extends FragmentActivity implements OnMapReadyCallback, View.OnClickListener {

    private GoogleMap mMap;
    Button start_location, destination_location, show_routes;
    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    String TAG = "Maps2Activity";
    int m = 0;
    double start_lat = 0, start_long = 0, dest_lat = 0, dest_long = 0;
    AutocompleteFilter typeFilter;

    LocationManager locationManager;
    LocationListener locationListener;
    Location l;
    LatLng userLocation;


    //Checking location permission
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 1) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_route);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        start_location = findViewById(R.id.start_location);
        destination_location = findViewById(R.id.destination_location);
        show_routes = findViewById(R.id.show_routes);
        start_location.setOnClickListener(this);
        destination_location.setOnClickListener(this);
        show_routes.setOnClickListener(this);

        typeFilter = new AutocompleteFilter.Builder()       //Filter place search results by country
                .setCountry("IN")
                .build();
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                mMap.clear();
                //Setting map to user's current location
                userLocation = new LatLng(location.getLatitude(), location.getLongitude());
                mMap.addMarker(new MarkerOptions().position(userLocation).title("Your Location"));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(userLocation));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(userLocation, 12.0f));

                start_lat = userLocation.latitude;
                start_long = userLocation.longitude;
                start_location.setText("Start from : My Current Location");
                Toast.makeText(FindRouteActivity.this,"Start : "+start_lat+" "+start_long,Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };


        //Location Permission Check
        if (Build.VERSION.SDK_INT < 23) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
        } else {
            if (ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},1);
            } else {

                //Getting user's location
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
                Location lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

                //mMap.clear();
                userLocation = new LatLng(lastKnownLocation.getLatitude(), lastKnownLocation.getLongitude());

                //Setting variables to current location
                start_lat = userLocation.latitude;
                start_long = userLocation.longitude;
                start_location.setText("Start from : My Current Location");
                Toast.makeText(this,"Start : "+start_lat+" "+start_long,Toast.LENGTH_SHORT).show();


                mMap.addMarker(new MarkerOptions().position(userLocation).title("Your Location"));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(userLocation));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(userLocation, 12.0f));
            }
        }

    }

    @Override
    public void onClick(View v) {
        if (v == start_location) {
            m=1;
            try {

                //Opening Place Autocomplete search
                Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN).setFilter(typeFilter)
                        .build(this);
                startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
            } catch (GooglePlayServicesRepairableException e) {
                // TODO: Handle the error.
            } catch (GooglePlayServicesNotAvailableException e) {
                // TODO: Handle the error.
            }
        }

        else if (v == destination_location) {
            m=2;
            try {
                //Opening Place Autocomplete search
                Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN).setFilter(typeFilter)
                        .build(this);
                startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
            } catch (GooglePlayServicesRepairableException e) {
                // TODO: Handle the error.
            } catch (GooglePlayServicesNotAvailableException e) {
                // TODO: Handle the error.
            }
        }
        else if (v==show_routes) {
            //Show routes button clicked
            if (start_lat==0 || start_long==0) {
                Toast.makeText(this,"Enter Start Location",Toast.LENGTH_SHORT).show();
            } else if (dest_lat == 0 || dest_long == 0) {
                Toast.makeText(this, "Enter Destination Location", Toast.LENGTH_SHORT).show();
            } else {
                /*Intent i = new Intent(this,ShowRoutes.class);
                i.putExtra("start_lat",start_lat);
                i.putExtra("start_long",start_long);
                i.putExtra("dest_lat",dest_lat);
                i.putExtra("dest_long",dest_long);
                startActivity(i);*/
                Toast.makeText(this,"SHOW ROUTES",Toast.LENGTH_SHORT).show();
            }
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                Log.i(TAG, "Place: " + place.getName());
                LatLng position = place.getLatLng();
                double lat = position.latitude;
                double lon = position.longitude;
                if (m == 1) {
                    //start location button is clicked
                    locationManager.removeUpdates(locationListener);
                    mMap.clear();
                    start_location.setText("Start from : "+place.getAddress());
                    start_lat = lat;
                    start_long = lon;
                    Toast.makeText(this,"Start : "+start_lat+" "+start_long,Toast.LENGTH_SHORT).show();
                } else if (m == 2) {
                    //destination location button is clicked
                    destination_location.setText("Destination at : "+place.getAddress());
                    dest_lat = lat;
                    dest_long = lon;
                    Toast.makeText(this,"Destination : "+dest_lat+" "+dest_long,Toast.LENGTH_SHORT).show();
                }
                //Adding marker to selected place on place autocomplete search
                mMap.addMarker(new MarkerOptions().position(place.getLatLng()).title(place.getName().toString()));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(place.getLatLng()));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(place.getLatLng(), 12.0f));
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                Log.i(TAG, status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }


    }
