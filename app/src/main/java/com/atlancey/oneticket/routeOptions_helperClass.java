package com.atlancey.oneticket;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by DuDe DhRuVa on 25-08-2018.
 */
//this class is to help set up the values

public class routeOptions_helperClass extends ArrayAdapter<routeOptions_getterAndSetter>{

        private ArrayList<routeOptions_getterAndSetter> reviewList;
        private LayoutInflater vi;
        private int Resource;
        private ViewHolder holder;
        ImageView mainTravelType,individualTravelType;
        TextView costTotal,timeTotal,midSource,midDestination,midCost,midTime,midTravelNumber;
        int gen = 0;


        routeOptions_helperClass(Context context, int resource, ArrayList<routeOptions_getterAndSetter> objects)
        {
            super(context, resource, objects);
            vi = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            Resource = resource;
            this.reviewList = objects;

        }

        @Override
        public View getView(int position, View convertView, @NonNull ViewGroup parent) {

            View v = convertView;

            // if view is null then set below views
            // below is code of other projecty, copied to take referrence


            if (v == null) {
                holder = new ViewHolder();
                v = vi.inflate(Resource, null);

                //binding holder variables to xml views

                holder.mainTravelType = (ImageView) v.findViewById(R.id.mode_journey);
                holder.costTotal = (TextView) v.findViewById(R.id.tv_cost);
                holder.timeTotal = (TextView) v.findViewById(R.id.tv_time);
                holder.individualTravelType = (ImageView) v.findViewById(R.id.individual_travelImage);
                holder.tvusefullness = (TextView) v.findViewById(R.id.);
                holder.tvusefullness = (TextView) v.findViewById(R.id.usefulness);
                holder.tvusefullness = (TextView) v.findViewById(R.id.usefulness);
                holder.rbrating = (RatingBar) v.findViewById(R.id.ratingbar);
                v.setTag(holder);
            } else {
                holder = (ViewHolder) v.getTag();
            }

            //setting data to the holder vars from "Reviews" getters

            holder.tvtitle.setText(reviewList.get(position).getTitle());
            holder.tvcomment.setText(reviewList.get(position).getComment());
            holder.tvusefullness.setText("usefullness:" + reviewList.get(position).getUsefulness());

            holder.rbrating.setRating(Integer.parseInt(reviewList.get(position).getStars()));
            return v;

        }


        private static class ViewHolder {
            ImageView mainTravelType,individualTravelType;

            TextView costTotal,timeTotal;
            TextView tvcomment;
            TextView tvusefullness;
            RatingBar rbrating;
        }
    }



}
