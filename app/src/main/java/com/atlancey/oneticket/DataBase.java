
package com.atlancey.oneticket;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by DuDe DhRuVa on 09-08-2018.
 */

//use this class by:
    // Create an object of this class
// BataBase DB=new DataBase(that_activity_name.this);
public class DataBase extends SQLiteOpenHelper{

    private static final String DATA_BASE_NAME="Info_Register";
    private static final int database_version=1;

    private static final String Table_NAME_USER_INFO="USER INFO";

    private static final String Column_u1="User_ID";
    private static final String Column_u2="User_Name";
    private static final String Column_u3="User_Email";
    private static final String Column_u4="User_Password";
    private static final String Column_u4re="User_RE_Password";
    private static final String Column_u5="User_Credit";
    private static final String Column_u6="User_Contact";
    private static final String Column_u7="User_Gender";
    private static final String Column_u8="User_DOB";

    private static final String Table_NAME_TICKET_INFO="TICKETS_INFO";

    private static final String Column_t1="Ticket_Id";
    private static final String Column_t2="Ticket_Source";
    private static final String Column_t3="Ticket_Destination";
    private static final String Column_t4="Ticket_Type";
    private static final String Column_t5="Ticket_Status";
    private static final String Column_t6="Ticket_IssueTime";

    // Following values are for storing cost of travel

    private static final String Table_NAME_BUS_Fare="Bus_Fare";
    private static final String Column_b0="Station_name";
    private static final String Column_b1="Station_1_charge";
    private static final String Column_b2="Station_2_charge";
    private static final String Column_b3="Station_3_charge";
    private static final String Column_b4="Station_4_charge";
    private static final String Column_b5="Station_5_charge";
    private static final String Column_b6="Station_6_charge";

    public DataBase(Context context)
    {

        super(context,DATA_BASE_NAME,null,database_version);
        setIdleConnectionTimeout(Long.MAX_VALUE);


    }

    @Override
    public void onCreate(SQLiteDatabase ABC)
    {
        ABC.execSQL("Create Table " + Table_NAME_USER_INFO + "(User_id Text Primary key,User_name Text Not null,User_email Text not null," +
                "User_password Text not null,User_re_password Text not null,User_credit int,User_contact int,User_gender Text,User_Dob int) ");
        ABC.execSQL("Create Table " + Table_NAME_TICKET_INFO + "(Ticket_id Text Primary key,Ticket_source Text Not null," +
                "Ticket_destination Text Not null,Ticket_type Text Not null,Ticket_status int not null,Ticket_time Text )");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+Table_NAME_USER_INFO);
        sqLiteDatabase.execSQL("Drop table if exists "+Table_NAME_TICKET_INFO);

    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        super.onDowngrade(db, oldVersion, newVersion);
    }



    //to enter values at SignUp time  // also use same to store values after login
    // NOTE : only user info, not tickets info
    // use this as , call it by the object of this class and pass all the values as you see in argument list
    // BD.register_profile(id (i.e. returned from the server) , name,email,password,re password,credits,contact,gender,dob);
    public Boolean register_profile(String U_id,String U_name,String U_email,String U_password,String U_re_password,int U_credit,int U_contact,String U_gender,int U_dob)
    {
        try
        {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(Column_u1,U_id);
            values.put(Column_u2,U_name);
            values.put(Column_u3,U_email);
            values.put(Column_u4,U_password);
            values.put(Column_u4re,U_re_password);
            values.put(Column_u5,U_credit);
            values.put(Column_u6,U_contact);
            values.put(Column_u7,U_gender);
            values.put(Column_u8,U_dob);
            db.insertOrThrow(Table_NAME_USER_INFO, null, values);
            db.close();
        }
        catch (Exception ex)
        {
            Log.e("Error",ex.toString());
            return false;

        }
        return true;

    }

    //To display values in User Profile Activity , arg -> pass email (it is a value of any key to compare the data)
    // Name 2 , email 3 , credit 5, contact 6, gender 7, dob 8
    public String[] view_profile(String arg)
    { String values[]=new String[]{null,null,null,null,null,null};
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor C;
        String select_query="";

        C=db.query(Table_NAME_USER_INFO,new String[]{Column_u2},Column_u3+"=?",new String[]{arg},null,null,null,null);

        if(C!=null)
        {C.moveToNext();
            values[0]=C.getString(C.getColumnIndex(Column_u2));
        }

        C=db.query(Table_NAME_USER_INFO,new String[]{Column_u3},Column_u3+"=?",new String[]{arg},null,null,null,null);

        if(C!=null)
        {C.moveToNext();
            values[1]=C.getString(C.getColumnIndex(Column_u3));
        }

        C=db.query(Table_NAME_USER_INFO,new String[]{Column_u5},Column_u3+"=?",new String[]{arg},null,null,null,null);

        if(C!=null)
        {C.moveToNext();
            values[2]=C.getString(C.getColumnIndex(Column_u5));
        }

        C=db.query(Table_NAME_USER_INFO,new String[]{Column_u6},Column_u3+"=?",new String[]{arg},null,null,null,null);

        if(C!=null)
        {C.moveToNext();
            values[3]=C.getString(C.getColumnIndex(Column_u6));
        }

        C=db.query(Table_NAME_USER_INFO,new String[]{Column_u7},Column_u3+"=?",new String[]{arg},null,null,null,null);

        if(C!=null)
        {C.moveToNext();
            values[4]=C.getString(C.getColumnIndex(Column_u7));
        }

        C=db.query(Table_NAME_USER_INFO,new String[]{Column_u8},Column_u3+"=?",new String[]{arg},null,null,null,null);

        if(C!=null)
        {C.moveToNext();
            values[5]=C.getString(C.getColumnIndex(Column_u8));
        }


        C.close();
        db.close();
        // this return will pass a string array storing : name , email , credit, contact,gender,dob
        return(values);
    }

    //for saving all tickets returned from server and also when new ticket is generated,
    // then refresh and load newly made last 10/5 tickets (Taking 5)
    public Boolean tickets_save(String T_id,String T_source,String T_Destination,String T_Type,String T_Status,String T_Time)
    {

        try
        {
            SQLiteDatabase db = this.getWritableDatabase();
            db.delete(Table_NAME_TICKET_INFO,null,null);  // this will first delete the existing values and then load new values
            ContentValues values = new ContentValues();
            values.put(Column_t1,T_id);
            values.put(Column_t2,T_source);
            values.put(Column_t3,T_Destination);
            values.put(Column_t4,T_Type);
            values.put(Column_t5,T_Status);
            values.put(Column_t6,T_Time);
            db.insertOrThrow(Table_NAME_TICKET_INFO, null, values);
            db.close();
        }
        catch (Exception ex)
        {
            Log.e("Error",ex.toString());
            return false;

        }
        return true;
    }

    // For returning only the ticket id to display qr code, taking only 5 tickets
    public String[] view_tickets()
    { String values[]=new String[]{null,null,null,null,null};
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor C;

        C=db.query(Table_NAME_TICKET_INFO,new String[]{Column_t1},null,null,null,null,null,null);

        if(C!=null)
        {C.moveToNext();
            values[0]=C.getString(C.getColumnIndex(Column_t1));
        }
        if(C!=null)
        {C.moveToNext();
            values[1]=C.getString(C.getColumnIndex(Column_t1));
        }
        if(C!=null)
        {C.moveToNext();
            values[2]=C.getString(C.getColumnIndex(Column_t1));
        }
        if(C!=null)
        {C.moveToNext();
            values[3]=C.getString(C.getColumnIndex(Column_t1));
        }
        if(C!=null)
        {C.moveToNext();
            values[4]=C.getString(C.getColumnIndex(Column_t1));
        }

        C.close();
        db.close();
        // this return will pass a string array storing  only ticket id
        return(values);
    }

    //Delete all values when logged out
    public void removeAll()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(Table_NAME_TICKET_INFO, null, null);
        db.delete(Table_NAME_USER_INFO, null, null);
        db.close();
    }


}
